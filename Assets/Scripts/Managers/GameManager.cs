using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Stage
{
    StartMenu,
    GameStage,
    Shopping,
    Ending
}

public class GameManager : Singleton<GameManager>
{
    private static int _round;
    private static int _enemiesLeft;
    private static string _playerName;
    private static int _playerPoints;
    private static float _playerHealth;
    private static Stage _stage;
    private static CharacterSO _playerSO;
    
    public static int Round { get => _round; set => _round = value; }
    public static string PlayerName { get => _playerName; set => _playerName = value; }
    public static int PlayerPoints { get => _playerPoints; set => _playerPoints = value; }
    public static float PlayerHealth { get => _playerHealth; set => _playerHealth = value; }
    public static int EnemiesLeft { get => _enemiesLeft; set => _enemiesLeft = value; }
    public static Stage Stage { get => _stage; set => _stage = value; }
    public static CharacterSO PlayerSO { get => _playerSO; set => _playerSO = value; }

    protected override void Awake()
    {
        base.Awake();
        DontDestroyOnLoad(this);
    }
    public static void roundRestart()
    {
        Round = 0;
    }
}
