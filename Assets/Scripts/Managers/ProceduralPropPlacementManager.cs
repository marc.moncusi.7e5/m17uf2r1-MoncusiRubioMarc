using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class ProceduralPropPlacementManager : MonoBehaviour
{
    public Tilemap tilemap;
    public Transform parentObject;
    [SerializeField] private List<PropsSO> _props;
    private float _propTableTotalPercentage;

    private Dictionary<string, float> _propTable = new Dictionary<string, float>();
    private bool[,] _occupiedTiles;

    public UnityEvent OnFinished;

    void Awake()
    {
        _occupiedTiles = new bool[tilemap.size.x, tilemap.size.y];
        foreach (var _prop in _props)
        {
            _propTableTotalPercentage += _prop.chanceOutOf100;
        }

        _propTableTotalPercentage *= 9;
        _propTable.Add("Empty", 100*_propTableTotalPercentage/9*8 / _propTableTotalPercentage);
        foreach (var _prop in _props)
        {

            _propTable.Add(_prop.name, 100 * _prop.chanceOutOf100 / _propTableTotalPercentage);
        }
        RandomizeProps();
    }
    private void RandomizeProps()
    {

        for (int i = 0; i < tilemap.size.x; i++)
        {
            for (int j = 0; j < tilemap.size.y; j++)
            {
                while (!_occupiedTiles[i, j])
                {
                    
                    var name = RandomProp();
                    if (name == "Empty")
                    {
                        OccupyTiles(i, j, 1, 1);
                    }
                    else
                    {
                        PropsSO prop = _props.Find(item => item.name == name);
                        if (!CheckOccupancy(i, j, prop.XSize, prop.YSize))
                        {
                            OccupyTiles(i, j, prop.XSize, prop.YSize);
                            PlaceGameObject(prop.prop, new Vector2(tilemap.origin.x + i, tilemap.origin.y + j));
                        }
                    }
                }
            }
        }
    }
    private string RandomProp()
    {
        int percentile = Random.Range(0, 100);
        float accumulatedProbability = 0;
        foreach (KeyValuePair<string, float> prop in _propTable)
        {
            accumulatedProbability += prop.Value;
            if (percentile <= accumulatedProbability)
                return prop.Key;
        }
        return "Empty";
    }
    private void OccupyTiles(int startX, int startY, uint lenX, uint lenY)
    {
        for (int i = startX; i < startX + lenX; i++)
        {
            for (int j = startY; j < startY + lenY; j++)
            {
                _occupiedTiles[i, j] = true;
            }
        }
    }
    private bool CheckOccupancy(int startX, int startY, uint lenX, uint lenY)
    {
        if (startX + lenX >= _occupiedTiles.GetLength(0)) return true;
        if (startY + lenY >= _occupiedTiles.GetLength(1)) return true;
        for (int i = startX; i < startX + lenX; i++)
        {
            for (int j = startY; j < startY + lenY; j++)
            {
                if (_occupiedTiles[i, j])
                {
                    return true;
                }
            }
        }
        return false;
    }

    private void PlaceGameObject(GameObject prefab, Vector2 position)
    {
        Instantiate(prefab, (Vector3)position, Quaternion.identity, parentObject);
    }
}
