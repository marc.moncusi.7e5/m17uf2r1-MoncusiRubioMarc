using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScenesManager : MonoBehaviour
{
    public static void LoadStartMenu()
    {
        GameManager.Stage = Stage.StartMenu;
        SceneManager.LoadScene("StartScene");
    }

    public static void LoadGame()
    {
        GameManager.Stage = Stage.GameStage;
        GameManager.roundRestart();
        SceneManager.LoadScene("GameScene");
    }

    public static void Shopping()
    {
        GameManager.Stage = Stage.Shopping;
        SceneManager.LoadScene("ShoppingScene");
    }

    public static void EndMenu()
    {
        GameManager.Stage = Stage.Ending;
        SceneManager.LoadScene("GameOver");
    }
}
