using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum MusicByStage
{
    StartMenuMusic,
    BackgroundMusic,
    ShoppingMusic,
    EndingMusic
}

public class AudioManager : Singleton<AudioManager>
{
    public static AudioManager _audioManager;
    public AudioMixer _mixer;
    [SerializeField] AudioClip _startMenuMusic;
    [SerializeField] AudioClip[] _backgroundMusic;
    [SerializeField] AudioClip _shoppingMusic;
    [SerializeField] AudioClip _endingMusic;
    MusicByStage _musicByStage;

    AudioSource _audioSource;


    protected override void Awake()
    {
        base.Awake();
        _audioSource = GetComponent<AudioSource>();
        _musicByStage = MusicByStage.StartMenuMusic;

        DontDestroyOnLoad(this);
    }

    private void Update()
    {
        CheckMusic();
    }

    private AudioClip GetRandomSong(AudioClip[] songs)
    {
        return songs[Random.Range(0, songs.Length)];
    }

    public void SetMasterVolume(Slider volume)
    {
        _mixer.SetFloat("Master", volume.value);
    }

    public void SetMusicVolume(Slider volume)
    {
        _mixer.SetFloat("Music", volume.value);
    }

    public void SetSFXVolume(Slider volume)
    {
        _mixer.SetFloat("SFX", volume.value);
    }

    private void CheckMusic()
    {
        switch (GameManager.Stage)
        {
            case Stage.StartMenu:
                if (_musicByStage != MusicByStage.StartMenuMusic || !_audioSource.isPlaying)
                {
                    _musicByStage = MusicByStage.StartMenuMusic;
                    _audioSource.clip = _startMenuMusic;
                    _audioSource.Play();
                }
                break;
            case Stage.GameStage:
                if (_musicByStage != MusicByStage.BackgroundMusic || !_audioSource.isPlaying)
                {
                    _musicByStage = MusicByStage.BackgroundMusic;
                    _audioSource.clip = GetRandomSong(_backgroundMusic);
                    _audioSource.Play();
                }
                break;
            case Stage.Shopping:
                if (_musicByStage != MusicByStage.ShoppingMusic || !_audioSource.isPlaying)
                {
                    _musicByStage = MusicByStage.ShoppingMusic;
                    _audioSource.clip = _shoppingMusic;
                    _audioSource.Play();
                }
                break;
            case Stage.Ending:
                if (_musicByStage != MusicByStage.EndingMusic || !_audioSource.isPlaying)
                {
                    _musicByStage = MusicByStage.EndingMusic;
                    _audioSource.clip = _endingMusic;
                    _audioSource.Play();
                }
                break;
            default:
                _audioSource.clip = _startMenuMusic;
                _audioSource.Play();
                break;
        }
    }
}
