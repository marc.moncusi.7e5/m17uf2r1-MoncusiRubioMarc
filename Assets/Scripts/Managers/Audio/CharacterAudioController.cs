using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAudioController : MonoBehaviour
{
    [SerializeField] private AudioClip _playerDeath;
    public AudioSource _audioSource;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayCharAttackAudio(AudioClip audioClip)
    {
        _audioSource.PlayOneShot(audioClip);
    }
    public void PlayCharDeathAudio()
    {
        _audioSource.PlayOneShot(_playerDeath);
    }
    public float GetCharDeathAudioLength()
    {
        return _playerDeath.length;
    }
}
