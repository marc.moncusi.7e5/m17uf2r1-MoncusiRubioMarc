using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseColliders : MonoBehaviour
{
    [SerializeField] private Renderer _floorRender;
    private float _floorXMin, _floorXMax;
    private float _floorYMin, _floorYMax;
    [SerializeField] private Collider2D[] colliders;

    void Awake()
    {
        _floorXMin = _floorRender.bounds.center.x - _floorRender.bounds.size.x * 0.5f;
        _floorXMax = _floorRender.bounds.size.x * 0.5f + _floorRender.bounds.center.x;
        _floorYMin = _floorRender.bounds.center.y - _floorRender.bounds.size.y * 0.5f;
        _floorYMax = _floorRender.bounds.size.y * 0.5f + _floorRender.bounds.center.y;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        colliders = Physics2D.OverlapCircleAll(mousePos, 1);
        foreach (var collider in colliders)
        {
            Debug.Log(collider.ToString());
        }
    }
}
