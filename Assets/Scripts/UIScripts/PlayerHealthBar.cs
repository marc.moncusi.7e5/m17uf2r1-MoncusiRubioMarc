using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour
{
    [SerializeField] private Slider _healthSlider;
    private void Awake()
    {
        _healthSlider = GetComponent<Slider>();
    }

    void Update()
    {
        SetHealth();
    }
    private void SetHealth()
    {
        _healthSlider.value = GameManager.PlayerHealth;
    }
    public void SetMaxHealth(float health)
    {
        _healthSlider.maxValue = health;
    }
}
