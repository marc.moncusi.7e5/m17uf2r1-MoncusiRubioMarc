using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayButton : MonoBehaviour
{
    [SerializeField] Button _playButton;
    [SerializeField] Text _userName;

    private void Awake()
    {
        GameManager.Stage = Stage.StartMenu;
    }
    void Start()
    {
        _playButton.onClick.AddListener(StartGame);
    }

    private void StartGame()
    {
        if (_userName != null)
        {
            GameManager.PlayerName = _userName.text;
        }
        ScenesManager.LoadGame();
    }
}
