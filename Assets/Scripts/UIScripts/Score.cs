using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField] Text _text;
    void Update()
    {
        _text.text = getScore();
    }
    private string getScore()
    {
        return "POINTS " + GameManager.PlayerPoints.ToString();
    }
}
