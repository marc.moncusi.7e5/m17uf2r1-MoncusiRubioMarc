using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Character
{
    public PlayerHealthBar _phealth;
    private bool isDying = false;

    protected override void Awake()
    {
        base.Awake();
        _phealth.SetMaxHealth(_stats.maxHealth);
        if(GameManager.PlayerSO == null)
        {
            GameManager.PlayerSO = _stats;
        }
        else
        {
            Debug.Log(GameManager.PlayerSO.currentHealth);
            _stats = GameManager.PlayerSO;
        }
        GameManager.PlayerHealth = _stats.currentHealth;
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
        GameManager.PlayerHealth = _stats.currentHealth;
        GameManager.PlayerSO = _stats;
        if (Input.GetKey(KeyCode.Mouse0)) HandleAttack(Camera.main.ScreenToWorldPoint(Input.mousePosition));
        if (Input.GetKeyDown(KeyCode.Mouse1)) GetComponent<Dash>().DashAbility(Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position);
    }
    public override void OnDie()
    {
        if (isDying) return;
        isDying = true;

        base.OnDie();
        ScenesManager.EndMenu();
    }
}
