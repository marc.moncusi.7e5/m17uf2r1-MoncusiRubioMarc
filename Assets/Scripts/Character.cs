using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour, IDamagable, IStunnable
{

    [SerializeField] public CharacterSO _stats;
    [SerializeField] protected Character _character;

    [SerializeField] protected AimBehaviour _characterAim;
    [SerializeField] protected AttackBehaviour _characterAttack;
    [SerializeField] protected MovementBehaviour _characterMovement;
    [SerializeField] protected CharacterAudioController _characterAudioController;

    public float damage { get; set; }
    public bool IsStunned { get; set; }

    protected virtual void Awake()
    {
        _stats = ScriptableObject.Instantiate(_stats);
        _character = this;
        _characterAim = GetComponentInChildren<AimBehaviour>();
        _characterAttack = GetComponentInChildren<AttackBehaviour>();
        _characterMovement = GetComponent<MovementBehaviour>();
        _characterAudioController = GetComponent<CharacterAudioController>();
    }

    protected virtual void Update()
    {
    }

    public virtual void HandleAttack(Vector3 position)
    {
        if (!IsStunned)
            _characterAttack.Attack(position);
    }

    public virtual void HandleMovement(Vector2 movement)
    {
        if (!IsStunned)
            _characterMovement.Move(movement);
    }

    public virtual void HandleAimOrientation(Vector3 orientation)
    {
        if (!IsStunned)
            _characterAim.AimOrientation(orientation);
    }

    public void ApplyDamage(float damageTaken)
    {
        _stats.currentHealth -= damageTaken;
        if (_stats.currentHealth <= 0) OnDie();
    }

    public void ApplyStun(float stunTime)
    {
        StartCoroutine(Stunned(stunTime));
    }

    IEnumerator Stunned(float stunTime)
    {
        IsStunned = true;
        yield return new WaitForSeconds(stunTime);
        IsStunned = false;
    }

    public virtual void OnDie()
    {
        GameManager.PlayerPoints += _stats.pointsOnDefeat;
        _characterAudioController.PlayCharDeathAudio();

        StartCoroutine(DestroyAfterAudio());
    }

    private IEnumerator DestroyAfterAudio()
    {
        yield return new WaitForSeconds(_characterAudioController.GetCharDeathAudioLength());

        Destroy(_stats);
        Destroy(gameObject);

    }
}
 