using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

enum SceneStatus
{
    Empty,
    EnemiesInScene,
    Finished
}

public class Spawner : MonoBehaviour
{
    int _rounds = 3;
    int _pointsOnRoundClear = 100;
    int _pointsOnSceneClear = 500;

    [SerializeField] private List<GameObject> _enemies;
    [SerializeField] private Renderer _floorRender;
    private SceneStatus _status;
    private Vector2 _spawnPosition;
    private float _floorXMin, _floorXMax;
    private float _floorYMin, _floorYMax;
    [SerializeField] private Collider2D[] colliders;
    // Start is called before the first frame update
    void Awake()
    {
        _floorXMin = _floorRender.bounds.center.x - _floorRender.bounds.size.x * 0.5f;
        _floorXMax = _floorRender.bounds.size.x * 0.5f + _floorRender.bounds.center.x;
        _floorYMin = _floorRender.bounds.center.y - _floorRender.bounds.size.y * 0.5f;
        _floorYMax = _floorRender.bounds.size.y * 0.5f + _floorRender.bounds.center.y;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_status)
        {
            case SceneStatus.Empty:
                SpawnArea();
                SpawnEnemies();
                _status = SceneStatus.EnemiesInScene;
                break;
            case SceneStatus.Finished:

                break;
            case SceneStatus.EnemiesInScene:
                if (GameManager.EnemiesLeft <= 0)
                {
                    if (GameManager.Round >= _rounds)
                    {
                        GameManager.PlayerPoints += _pointsOnSceneClear;
                        _status = SceneStatus.Finished;
                        ScenesManager.Shopping();
                    }
                    else
                    {
                        _status = SceneStatus.Empty;
                        GameManager.PlayerPoints += GameManager.Round * _pointsOnRoundClear;
                        GameManager.Round++;
                    }
                }
                break;
            default:
                break;
        }
    }
    void SpawnArea()
    {
        Vector2 spawnPoint;
        do
        {
            var x = Random.Range(_floorXMin, _floorXMax);
            var y = Random.Range(_floorYMin, _floorYMax);
            spawnPoint = new Vector2(x, y);
        } while (!SpawnPossible(spawnPoint));

        _spawnPosition = spawnPoint;
    }
    void SpawnEnemies()
    {
        for (int i = 0; i < GameManager.Round * 3; i++)
        {
            Vector2 spawnPoint;
            do
            {
                spawnPoint = _spawnPosition + Random.insideUnitCircle;
            } while (!SpawnPossible(spawnPoint));
            Instantiate(_enemies[(int)Mathf.Floor(Random.Range(0, _enemies.Count))], spawnPoint, new Quaternion(0, 0, 0, 0));
            GameManager.EnemiesLeft++;
        }
    }
    bool SpawnPossible(Vector2 position)
    {
        //colliders = Physics2D.OverlapCircleAll(position, Mathf.Max(_floorXMax * 2, _floorYMax * 2));
        colliders = Physics2D.OverlapCircleAll(position, 1);
        if (_floorXMin > position.x && position.x > _floorXMax)
        {
            return false;
        }
        if (_floorYMin > position.y && position.y > _floorYMax)
        {
            return false;
        }

        foreach (Collider2D collider in colliders)
        {
            if (collider.OverlapPoint(position))
            {
                return false;
            }
        }
        return true;
    }
}
