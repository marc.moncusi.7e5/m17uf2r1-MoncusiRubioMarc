using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : Enemy
{
    protected override void Update()
    {
        base.Update();
        _characterAttack = GetComponent<AttackBehaviour>();
        _characterAim = GetComponent<AimBehaviour>();
    }
}
