using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : Character
{
    [SerializeField]
    protected AIDetection _detector;
    [SerializeField]
    protected PatrolBehaviour _characterPatrol;
    private bool isDying = false;

    protected override void Awake()
    {
        base.Awake();
        _characterPatrol = GetComponentInChildren<PatrolBehaviour>();
        _detector = GetComponentInChildren<AIDetection>();
    }

    protected override void Update()
    {
        base.Update();
        if (_detector.TargetVisible)
        {
            HandleAimOrientation(_detector.Target.position);
            HandleAttack(_detector.Target.position);
        }
        else
        {
            _characterPatrol.Patrol(_character);
        }
    }
    public override void OnDie()
    {
        if (isDying) return;
        isDying = true;

        base.OnDie();
        GameManager.EnemiesLeft--;
    }
}
