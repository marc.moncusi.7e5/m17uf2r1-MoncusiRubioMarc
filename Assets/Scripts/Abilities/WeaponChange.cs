using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponChange : MonoBehaviour
{
    public WeaponSO[] weapons = new WeaponSO[5];
    private int _currentIndex = 0;
    public int CurrentIndex
    {
        get
        {
            if (_currentIndex > weapons.Length - 1) { _currentIndex = 0; }
            if (_currentIndex < 0) { _currentIndex = weapons.Length - 1; }
            return _currentIndex;
        }
        set { _currentIndex = value; }
    }

    // Start is called before the first frame update
    void Awake()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0f || Input.GetKeyDown(KeyCode.C))
        {
            MovePrevious();
            ChangeClass();
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0f || Input.GetKeyDown(KeyCode.X))
        {
            MoveNext();
            ChangeClass();
        }
    }
    private int MoveNext()
    {
        _currentIndex++;
        return CurrentIndex;
    }

    private int MovePrevious()
    {
        _currentIndex--;
        return CurrentIndex;
    }
    private void ChangeClass()
    {
        //GetComponent<AttackBehaviour>().weapon = weapons[_currentIndex];
    }
}
