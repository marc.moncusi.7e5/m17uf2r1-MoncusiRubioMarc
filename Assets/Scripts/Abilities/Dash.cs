using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AbilityState
{
    Available,
    InCooldown,
    Desactivated
}

public class Dash : MonoBehaviour
{
    private float _boost = 60f;
    private AbilityState _state;
    private float _currentCooldown = 0.0f;
    private float _maxCooldown = 2.0f;

    private Rigidbody2D _rigidbody;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _state = AbilityState.InCooldown;
    }

    void Update()
    {
        if (_state == AbilityState.InCooldown)
        {
            _currentCooldown -= Time.deltaTime;
            if (_currentCooldown <= 0.0f)
            {
                _state = AbilityState.Available;
            }
        }
    }
    public void DashAbility(Vector3 direction)
    {
        if (_state == AbilityState.Available)
        {
            _rigidbody.AddForce(direction * _boost, ForceMode2D.Impulse);
            _currentCooldown = _maxCooldown;
            _state = AbilityState.InCooldown;
        }

    }
}
