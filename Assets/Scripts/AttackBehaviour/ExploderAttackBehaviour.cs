using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExploderAttackBehaviour : AttackBehaviour
{
    private MeleeWeaponSO _mWeaponData;
    private float _impulseForce = 30f;

    public override void Awake()
    {
        base.Awake();
        _mWeaponData = (MeleeWeaponSO)weaponData;
    }
    public override void Attack(Vector3 position)
    {
        transform.position = Vector2.MoveTowards(transform.position, position, _mWeaponData.speed * Time.deltaTime);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            this.GetComponent<Collider2D>().enabled = false;
            IDamagable damagable = collision.GetComponent<IDamagable>();
            if (damagable != null)
            {
                damagable.ApplyDamage(_mWeaponData.damage);
            }
            SoundEffect();
            Vector2 direction = collision.transform.position - transform.position;
            collision.GetComponent<Rigidbody2D>().AddForce(direction * _impulseForce, ForceMode2D.Impulse);
            var character = this.GetComponent<Character>();
            character._stats.pointsOnDefeat = 0;
            character.OnDie();
        }
    }
}
