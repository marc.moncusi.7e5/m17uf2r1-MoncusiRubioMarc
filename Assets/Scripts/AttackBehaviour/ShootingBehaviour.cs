using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ShootingBehaviour : AttackBehaviour
{
    private RangedWeaponSO rWeaponData;
    private AttackState _state;
    private float _currentCooldown = 0.0f;
    private int _currentMagazine = 0;

    [SerializeField] private Collider2D[] _selfColliders;

    public override void Awake()
    {
        base.Awake();
        rWeaponData = (RangedWeaponSO)weaponData;
        _selfColliders = this.GetComponentsInParent<Collider2D>();
        _state = AttackState.InCooldown;
    }

    // Update is called once per frame
    void Update()
    {
        if (_state == AttackState.InCooldown)
        {
            _currentCooldown -= Time.deltaTime;
            if (_currentCooldown <= 0.0f)
            {
                _state = AttackState.Available;
                _currentMagazine = rWeaponData.magazine;
            }
        }
    }

    public override void Attack(Vector3 position)
    {
        if (_state == AttackState.Available)
        {
            SoundEffect();
            GameObject projectile = Instantiate(rWeaponData.projectileData.projectilePrefab);
            projectile.transform.position = this.gameObject.transform.position;
            projectile.transform.localRotation = this.gameObject.transform.rotation;
            projectile.GetComponent<Projectile>().Initialize(rWeaponData.projectileData);
            foreach(var _selfCollider in _selfColliders) Physics2D.IgnoreCollision(projectile.GetComponent<Collider2D>(), _selfCollider);
            _currentMagazine--;
            if (_currentMagazine == 0)
            {
                _state = AttackState.InCooldown;
                _currentCooldown = rWeaponData.cooldown;
            }
        }
    }
}
