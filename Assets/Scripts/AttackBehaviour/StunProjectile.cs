using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StunProjectile : Projectile
{
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        IStunnable stunnable = collision.GetComponent<IStunnable>();

        if (stunnable != null)
        {
            stunnable.ApplyStun(projectileData.damage);
        }
        Destroy(gameObject);
    }
}
