using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public ProjectileSO projectileData;
    private Rigidbody2D _rigidbody;
    [SerializeField]private Animator _animator;

    public void Initialize(ProjectileSO projectileData)
    {
        this.projectileData = projectileData;
        _rigidbody.velocity = transform.up * this.projectileData.speed;
    }
    private void Awake()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();
    }

    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        IDamagable damagable = collision.GetComponent<IDamagable>();
        if (damagable != null)
        {
            damagable.ApplyDamage(projectileData.damage);
        }
        destroy();
    }
    protected void destroy()
    {
        Destroy(_rigidbody);
        Destroy(GetComponent<Collider2D>());
        _animator.SetTrigger("Extinction");
        Destroy(gameObject, _animator.GetCurrentAnimatorStateInfo(0).length);
    }
}
