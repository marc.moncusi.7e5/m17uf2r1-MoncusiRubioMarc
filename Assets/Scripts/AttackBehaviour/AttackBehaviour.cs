using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackState
{
    Available,
    InCooldown
}
public abstract class AttackBehaviour : MonoBehaviour
{
    public WeaponSO weaponData;
    public abstract void Attack(Vector3 position);
    protected CharacterAudioController _characterAudioController;

    public virtual void Awake()
    {
        _characterAudioController = GetComponentInParent<CharacterAudioController>();
    }
    public virtual void SoundEffect()
    {
        _characterAudioController.PlayCharAttackAudio(weaponData.WeaponAudio);
    }
}
