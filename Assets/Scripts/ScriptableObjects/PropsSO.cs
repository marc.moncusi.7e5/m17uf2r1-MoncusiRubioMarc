using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewDecoration", menuName = "ScriptableObjects/Decoration")]
public class PropsSO : ScriptableObject
{
    [Header("Prop data:")]
    public GameObject prop;
    public uint XSize;
    public uint YSize;
    public float chanceOutOf100; 
}
