using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Itemtype
{
    Weapon,
    consumable,
    PowerUp,
    Bullets
}

[CreateAssetMenu(fileName = "NewItem", menuName = "ScriptableObjects/Item")]
public class ItemSO : ScriptableObject
{
    public string itemName;
    public string description;
    public float price;
    public float chance;
    public Itemtype type;
    public Sprite sprite;
}
