using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewMeleeWeapon", menuName = "ScriptableObjects/MeleeWeapon")]

public class MeleeWeaponSO : WeaponSO
{
    public float speed;
    public float damage;
}

