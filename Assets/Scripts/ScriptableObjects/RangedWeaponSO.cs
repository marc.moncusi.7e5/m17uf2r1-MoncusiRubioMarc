using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewRangedWeapon", menuName = "ScriptableObjects/RangedWeapon")]

public class RangedWeaponSO : WeaponSO
{
    public ProjectileSO projectileData;
    public int magazine;
    public float cooldown;
}
