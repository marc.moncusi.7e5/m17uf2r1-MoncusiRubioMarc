using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewProjectile", menuName = "ScriptableObjects/Projectile")]
public class ProjectileSO : ScriptableObject
{
    public GameObject projectilePrefab;
    public float speed;
    public float damage;
}

