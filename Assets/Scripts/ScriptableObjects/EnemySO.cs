using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEnemy", menuName = "ScriptableObjects/Enemies")]
public class EnemySO : CharacterSO
{
    public float DetectionRadius;
}
