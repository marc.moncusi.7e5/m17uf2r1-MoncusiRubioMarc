using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAimBehaviour : AimBehaviour
{
    public override void AimOrientation(Vector3 orientation)
    {
        _aimDirection = orientation - transform.position;
        _desiredAngle = Mathf.Atan2(_aimDirection.y, _aimDirection.x) * Mathf.Rad2Deg -90;
        var _rotationStep = RotationSpeed * Time.deltaTime;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.Euler(0, 0, _desiredAngle), _rotationStep);
    }
}
