using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class AimBehaviour : MonoBehaviour
{
    [SerializeField] protected float RotationSpeed = 150;
    [SerializeField] protected Vector3 _aimDirection;
    [SerializeField] protected float _desiredAngle;
    public float desiredAngle { get => _desiredAngle; set => _desiredAngle = value; }
    public abstract void AimOrientation(Vector3 orientation);
}
