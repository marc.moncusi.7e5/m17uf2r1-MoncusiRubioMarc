using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseAimBehaviour : AimBehaviour
{
    public override void AimOrientation(Vector3 orientation)
    {
        transform.rotation = Quaternion.LookRotation(Vector3.forward, orientation - transform.position);
    }

    private void Update()
    {
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        AimOrientation(mousePos);
        
    }
}
