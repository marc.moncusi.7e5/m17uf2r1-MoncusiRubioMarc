using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public ItemSO weaponData;
    [SerializeField] protected AimBehaviour _weaponAim;
    [SerializeField] protected AttackBehaviour _weaponAttack;

    protected virtual void Awake()
    {
        if (_weaponAim == null)
        {
            _weaponAim = GetComponent<AimBehaviour>();
        }
        if (_weaponAttack == null)
        {
            _weaponAttack = GetComponent<AttackBehaviour>();
        }
        this.GetComponent<SpriteRenderer>().sprite = weaponData.sprite;
    }

    public virtual void HandleAttack(Vector3 position)
    {
        _weaponAttack.Attack(position);

    }
    public virtual void HandleAimOrientation(Vector3 orientation)
    {
        _weaponAim.AimOrientation(orientation);
    }
}
