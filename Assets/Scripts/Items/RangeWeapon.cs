using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : Weapon
{
    protected override void Awake()
    {
        base.Awake();
        weaponData = (RangedWeaponSO)weaponData;
    }
}
