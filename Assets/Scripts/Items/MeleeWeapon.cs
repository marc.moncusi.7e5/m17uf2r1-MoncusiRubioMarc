using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    protected override void Awake()
    {
        base.Awake();
        weaponData = (MeleeWeaponSO)weaponData;
    }
}
