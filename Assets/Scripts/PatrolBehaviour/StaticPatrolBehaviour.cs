using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticPatrolBehaviour : PatrolBehaviour
{
    public float patrolDelay = 4;

    [SerializeField]
    private Vector2 randomDirection = Vector2.zero;
    [SerializeField]
    private float currentPatrolDelay;


    private void Awake()
    {
        randomDirection = Random.insideUnitCircle;
    }
    public override void Patrol(Character _character)
    {
        float angle = Vector2.Angle(_character.transform.up, randomDirection);
        if (currentPatrolDelay <= 0 && (angle < 2))
        {
            randomDirection = Random.insideUnitCircle;
            currentPatrolDelay = patrolDelay;
        }
        else
        {
            if (currentPatrolDelay > 0)
                currentPatrolDelay -= Time.deltaTime;
            else
                _character.HandleAimOrientation((Vector2)_character.transform.position + randomDirection);
        }
    }

}
