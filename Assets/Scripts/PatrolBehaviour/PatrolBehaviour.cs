using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PatrolBehaviour : MonoBehaviour
{
    public abstract void Patrol(Character _character);
}
