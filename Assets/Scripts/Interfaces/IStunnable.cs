using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStunnable
{
    public bool IsStunned { get; set; }
    void ApplyStun(float stunTaken);
}