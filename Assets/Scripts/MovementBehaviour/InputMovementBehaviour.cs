using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputMovementBehaviour : MovementBehaviour
{
    public Animator animator;
    private float _inputX, _inputY;
    private float _maxSpeed = 15.0f; //This stats have to change someday
    private Rigidbody2D _rigidbody;
    private int _acceleration = 20;

    public override void Move(Vector2 movement)
    {
        _rigidbody.velocity += _acceleration * movement * Time.deltaTime;

        if (_rigidbody.velocity.magnitude > _maxSpeed)
        {
            _rigidbody.velocity = Vector2.ClampMagnitude(_rigidbody.velocity, _maxSpeed);
        }
    }

    void Awake()
    {
        _rigidbody = this.GetComponent<Rigidbody2D>();
        _inputX = 0;
        _inputY = 0;
    }

    private void FixedUpdate()
    {
        _inputX = Input.GetAxis("Horizontal");
        _inputY = Input.GetAxis("Vertical");
        Move(new Vector2(_inputX, _inputY).normalized);

    }
}
